const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const post = mongoose.model("post")
const user = mongoose.model('user')


router.get('/user/:id', requireLogin, (req, res) => {
    // console.log(id)
    user.findOne({ _id: req.params.id })
        .then(user => {
            post.find({ postedBy: req.params.id })
                .populate("postedBy", "_id name")
                .exec((err, post) => {
                    if (err) {
                        res.status(422).json({ error: err })
                    }
                    res.json({ user, post })
                })
        }).catch(err => {
            return res.status(404).json({ message: "user not found" })
        })

})

router.put('/follow', requireLogin, (req, res) => {
    user.findByIdAndUpdate(req.body.followId, {
        $push: { followers: req.user._id }
    }, {
        new: true
    }, (err, result) => {
        if(err){
        res.status(422).json({ err: err })
    }
        user.findByIdAndUpdate(req.user._id, {
            $push: { following: req.body.followId }
        }, {
            new: true
        }).select("-password").then(result=>{res.json(result)})
        .catch(err=>{
            return res.status(422).json({err:err})
        })
    }
    )
})

router.put('/unfollow', requireLogin, (req, res) => {
    user.findByIdAndUpdate(req.body.unfollowId, {
        $pull: { followers: req.user._id }
    }, {
        new: true
    }, (err, result) => {
        if(err){
        res.status(422).json({ err: err })
    }
        user.findByIdAndUpdate(req.user._id, {
            $pull: { following: req.body.unfollowId }
        }, {
            new: true
        }).select("-password").then(result=>{res.json(result)})
        .catch(err=>{
            return res.status(422).json({err:err})
        })
    }
    )
})
router.put('/profilepicupload',requireLogin,(req,res)=>{
    user.findByIdAndUpdate(req.user._id,{$set:{pic:req.body.pic}},
    {
        new:true
    },
    (err,result)=>{
        if(err)
        {
            return res.status(422).json({err:err})
        }
        res.json(result)
    
    })
})


module.exports = router