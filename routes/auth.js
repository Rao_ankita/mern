const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
const user = mongoose.model('user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const{JWT_TOKEN} = require('../config/key')
const requireLogin = require('../middleware/requireLogin')

router.get('/protected',requireLogin,(req,res)=>{
    res.send('hello user')
})

router.post('/signup', (req, res) => {
    const { name, email, password,pic } = req.body
    if (!name || !email || !password) {
        return res.status(422).json({ error: "please fill all the details" })
    }
    user.findOne({ email: email }).then((saveduser) => {
        if (saveduser) {
            return res.status(422).json({ error: "email exists" })
        }
        bcrypt.hash(password, 12)
            .then(hashedpassword => {
                const User = new user({
                    name,
                    email,
                    password:hashedpassword,
                    pic
                })
                User.save()
                    .then(User => {
                        res.json({ message: "data saved" })
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })

    })

        .catch(err => {
            console.log(err)
        })
})


router.post('/signin',(req,res)=>{
    const{email,password} = req.body
    if(!email||!password){
        res.status(422).json({message:'please fill all details'})
    }
    user.findOne({email,email})
    .then(saveduser=>{
        if(!saveduser){
            res.status(422).json({message:'invalid email or password'})  
        }
        bcrypt.compare(password,saveduser.password)
        .then(domatch=>{
            if(domatch){
               // res.status(422).json({message:'successfully signed in'}) 
               const token = jwt.sign({_id:saveduser._id},JWT_TOKEN)
              // console.log("token is:",token) 
               const {_id,name,email,followers,following,pic} = saveduser
               res.json({token,user:{_id,name,email,followers,following,pic}})
            }else{
                res.status(422).json({message:'invalid email or password'})     
            }
        })
        .catch(error=>{
            console.log(error)
        })
    })
})
module.exports = router;