const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const post = mongoose.model("post")

router.get('/allpost', requireLogin, (req, res) => {
    post.find()
        .populate("postedBy", "_id name")
        .populate("comments.postedBy", "_id name")
        .then(posts => {
            res.json({ posts })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/subpost', requireLogin, (req, res) => {
    post.find({postedBy:{$in:req.user.following}})
        .populate("postedBy", "_id name")
        .populate("comments.postedBy", "_id name")
        .then(posts => {
            res.json({ posts })
        })
        .catch(err => {
            console.log(err)
        })
})


router.post('/createpost', requireLogin, (req, res) => {
    const { title, body, pic } = req.body
    console.log(title, body, pic)
    if (!title || !body || !pic) {
        return res.status(422).json({ message: "please fill all details" })
    }
    req.user.password = undefined
    const Post = new post({
        title,
        body,
        photo: pic,
        postedBy: req.user
    })

    Post.save().then(result => {
        res.json({ post: result })
    }).catch(err => {
        console.log(err)
    })
})

router.get('/mypost', requireLogin, (req, res) => {
    // console.log(req.user)
    post.find({ postedBy: req.user._id })
        .populate("postedBy", "_id name")
        .then(mypost => {
            res.json({ mypost })
        })
        .catch(err => {
            console.log(err)
        })
})

router.put('/like', requireLogin, (req, res) => {
    post.findByIdAndUpdate(req.body.postId, {
        $push: { likes: req.user._id }
    }, {
        new: true
    }).exec((err, result) => {
        if (err) {
            return res.status(422).json({ error: err })
        } else {
            res.json(result)
        }
    })
})

router.put('/unlike', requireLogin, (req, res) => {
    post.findByIdAndUpdate(req.body.postId, {
        $pull: { likes: req.user._id }
    }, {
        new: true
    }).exec((err, result) => {
        if (err) {
            return res.status(422).json({ error: err })
        } else {
            res.json(result)
        }
    })
})

router.put('/comment', requireLogin, (req, res) => {
    const comment = {
        text: req.body.text,
        postedBy: req.user._id
    }
    post.findByIdAndUpdate(req.body.postId, {
        $push: { comments: comment }
    }, {
        new: true
    })
        .populate("comments.postedBy", "_id name")
        .populate("postedBy", "_id name")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            } else {
                res.json(result)
            }
        })
})


router.delete('/delete/:postId', requireLogin, (req, res) => {
    post.findOne({ _id: req.params.postId })
        .populate("postedBy", "_id")
        .exec((err, post) => {
            if (err || !post) {
                return res.status(422).json({ err: err })
            }
            if (post.postedBy._id.toString() === req.user._id.toString()) {
                post.remove()
                    .then(result => {
                        res.json(result)
                    }).catch(err => {
                        console.log(err)
                    })
                }
    })
})

router.delete('/delete/:postId/:commentId',requireLogin,(req,res)=>{
 post.findById(req.params.postId)
        .exec((err, post) => {
            if (err) {
                return res.status(422).json({ err: err })
            }
            if(post.comments.commentId.toString() !==req.params.commentId.toString())
            {
                return res.json({message:"no comments available"})
            }
            if (post.postedBy._id.toString() === req.user._id.toString()) {
                post.comments.commentId.remove()
                    .then(result => {
                        res.json(result)
                    }).catch(err => {
                        console.log(err)
                    })
                }
    })
    


})


module.exports = router