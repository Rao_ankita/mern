import React,{useContext} from 'react'
import {Link,useHistory} from 'react-router-dom'
import {UserContext} from '../App'
const NavBar = ()=>{
  const {state,dispatch} = useContext(UserContext)
  const history = useHistory()
  const renderList = () =>{
    if(state){
      return [
        <li><Link to="/Profile">Profile</Link></li>,
        <li><Link to="/createpost">create Post</Link></li>,
        <li><Link to="/followerspost">Following Post</Link></li>,
         <li>
          <button className="waves-effect waves-light btn #7cb342 light-green darken-1"
          onClick={() => {
            localStorage.clear()
            dispatch({type:"CLEAR"})
            history.push('/signup')
          }}>Logout</button>
        </li>
      ]
    }else{
      return[
      <li><Link to="/signin">Login</Link></li>,
        <li><Link to="/signup">Signup</Link></li>
      ]
    }
  }
    return(
        <nav>
    <div className="nav-wrapper white">
      <Link to={state?"/":"/signin"} className="brand-logo left">Instagram</Link>
      <ul id="nav-mobile" className="right hide-on-med-and-down">
        {renderList()}
      </ul>
    </div>
  </nav>
        
    )
}

export default NavBar