import React, { useEffect, useState,useContext } from 'react'
import {UserContext} from '../../App'
import {useParams} from 'react-router-dom'
const Profile = () => {
    const [userProfile, setProfile] = useState(null)
    
    const {state,dispatch} = useContext(UserContext)
    const {userId} = useParams()
    const [userFollow,setFollow] = useState(state? !state.following.includes(userId):true)
    console.log(userId)
    useEffect(() => {
        fetch(`/user/${userId}`, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            }
        }).then(res => res.json())
            .then(result => {
                console.log(result)
                setProfile(result)
            })
    }, [])

    const followUser = ()=>{
        fetch('/follow',{
            method:"put",
            headers:{
                "Content-Type":"application/json",
                "Authorization":'Bearer '+localStorage.getItem('jwt')
            },
            body:JSON.stringify({
                followId:userId
            })
        }).then(res=>res.json())
        .then(result=>{
            console.log(result)
            dispatch({type:"UPDATE",payload:{following:DataTransferItem.following,followers:DataTransferItem.followers}})
            localStorage.setItem("user",JSON.stringify(result))
            setProfile((prevStat)=>{
                return{
                ...prevStat,
                user:{
                    ...prevStat.user,
                    followers:[...prevStat.user.followers,result._id]
                }

            }
            })
            
        })
       
    }

    const unfollowUser = ()=>{
        fetch('/unfollow',{
            method:"put",
            headers:{
                "Content-Type":"application/json",
                "Authorization":'Bearer '+localStorage.getItem('jwt')
            },
            body:JSON.stringify({
                unfollowId:userId
            })
        }).then(res=>res.json())
        .then(result=>{
            console.log(result)
            dispatch({type:"UPDATE",payload:{following:DataTransferItem.following,followers:DataTransferItem.followers}})
            localStorage.setItem("user",JSON.stringify(result))
            setProfile((prevStat)=>{
                const newFollower = prevStat.user.followers.filter(item=>item!=result._id)
                return{
                ...prevStat,
                user:{
                    ...prevStat.user,
                    followers:newFollower
                }

            }
            })
            
        })
        
    }
    

    return (
        <>
        {userProfile ? 
        <div style={{ maxWidth: '550px', margin: '0px auto' }}>
        <div style={{
            display: "flex",
            justifyContent: "space-around",
            margin: "18px 0px",
            borderBottom: '1px solid grey'
        }}>
           
            
            <div>
                <h4>{userProfile.user.name}</h4>
                <h5>{userProfile.user.email}</h5>
                <div style={{ display: "flex", justifyContent: 'space-between', width: '120%' }}>
                  <h5>{userProfile.post.length}posts</h5>
                    <h5>{userProfile.user.followers.length} Followers </h5>
                    <h5>{userProfile.user.following.length}Following</h5>

                </div>
                {userFollow? <button 
                style={
                   { margin: '20px' }
                }
                className="waves-effect waves-light btn #7cb342 light-green darken-1"
          onClick={() => followUser()}>follow</button> :
          <button 
          style={
            { margin: '20px' }
         }
          className="waves-effect waves-light btn #7cb342 light-green darken-1"
          onClick={() => unfollowUser()}>unfollow</button>
          }
                
          
            </div>

        </div>
        <div className="gallery">
            {
            userProfile.post.map(item=>{
                return( 
                     <img key={item._id} className="item" src={item.photo}
                alt={item.title} /> 
                )
            })
        }
            
           
        </div>
    </div>
        :
        <h6>loading!!!!!!!</h6>}
        
        </>
    )
}

export default Profile