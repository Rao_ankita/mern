import React, { useState,useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import M from 'materialize-css'
const Signin = () => {
  const history = useHistory()
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [image, setImage] = useState("")
  const [url, setUrl] = useState(undefined)
  useEffect(()=>{
       if(url){
        uploadfields()
       }
  },[url])
  const uploadpic = () => {
    const data = new FormData()
    data.append("file", image)
    data.append("upload_preset", "inst-clone")
    data.append("cloud_name", "ankita1297")
    fetch("https://api.cloudinary.com/v1_1/ankita1297/image/upload", {
      method: "post",
      body: data
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setUrl(data.url)
      })
      .catch(err => {
        console.log(err)
      })
  }

  const uploadfields = ()=>{

    if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
      M.toast({ html: 'invalid email', classes: '#c62828 red darken-3' })
      return
    }
    fetch('/signup', {
      method: 'post',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name,
        email,
        password,
        pic:url
      })
    }).then(res => res.json())
      .then(data => {
        if (data.error) {
          M.toast({ html: data.error, classes: '#c62828 red darken-3' })
        }
        else {
          M.toast({ html: data.message, classes: '#388e3c green darken-2' })
          history.push('/signin')
        }
      }).catch(err => {
        console.log(err)
      })

  }

  const PostData = () => {
    if(image){
         uploadpic()
    }else{
      uploadfields()
    }
    
  }
  return (
    <div className="mycard">
      <div className="card auth-card input-field">
        <h2>Instagram</h2>
        <input type="text" placeholder="Name"
          value={name} onChange={(e) => setName(e.target.value)} />
        <input type="text" placeholder="Email"
          value={email} onChange={(e) => setEmail(e.target.value)} />
        <input type="password" placeholder="Password"
          value={password} onChange={(e) => setPassword(e.target.value)} />
        <div className="file-field input-field">
          <div className="btn #7cb342 light-green darken-1">
            <span>Upload Profile pic</span>
            <input type="file"
              onChange={(e) => setImage(e.target.files[0])} />
          </div>
          <div className="file-path-wrapper">
            <input className="file-path validate" type="text" />
          </div>


        </div>
        <button className="waves-effect waves-light btn #7cb342 light-green darken-1" onClick={() => PostData()}>signup</button>
        <h5><Link to="/signin"> have an account?</Link></h5>
      </div>
    </div>
  )
}

export default Signin