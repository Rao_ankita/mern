import React, { useEffect, useState,useContext } from 'react'
import {UserContext} from '../../App'
import { FloatingActionButton } from 'materialize-css'
const Profile = () => {
    const [mypics, setPics] = useState([])
    const [image, setImage] = useState("")
    const [url, setUrl] = useState("")
    const {state,dispatch} = useContext(UserContext)
    useEffect(() => {
        fetch('/mypost', {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            }
        }).then(res => res.json())
            .then(result => {
                setPics(result.mypost)
            })
    }, [])
useEffect(()=>{
    if(image){
        const data = new FormData()
        data.append("file", image)
        data.append("upload_preset", "inst-clone")
        data.append("cloud_name", "ankita1297")
        fetch("https://api.cloudinary.com/v1_1/ankita1297/image/upload", {
          method: "post",
          body: data
        })
          .then(res => res.json())
          .then(data => {
            console.log(data)
            // setUrl(data.url)
            // localStorage.setItem("user",JSON.stringify({...state,pic:data.url}))
            //  dispatch({type:'UPDATEPIC',payload:data.url})
             fetch('/profilepicupload',{
                 method:'put',
                 headers:{
                     "Content-Type":"application/json",
                     "Authorization":"Bearer "+localStorage.getItem('jwt')
                 },
                 body:JSON.stringify({
                     pic:data.url
                 })
             }).then(res=>res.json())
             .then(result=>{
                 console.log(result)
                 localStorage.setItem("user",JSON.stringify({...state,pic:result.pic}))
                 dispatch({type:'UPDATEPIC',payload:result.pic})
             })
        })
          .catch(err => {
            console.log(err)
          })
    }
},[image])
const uploadprofilepic = (file)=>{
    setImage(file)
    
}

    
    return (
        <div style={{ maxWidth: '550px', margin: '0px auto' }}>
            <div style={{
                display: "flex",
                justifyContent: "space-around",
                margin: "18px 0px",
                borderBottom: '1px solid grey'
            }}>
                <div >
                    <img style={{ width: '160px', height: '160px', borderRadius: '80px' }}
                        src={state?state.pic:"loading"}
                        alt="" />

               
          <div className="file-field input-field">
          <div className="btn #7cb342 light-green darken-1">
            <span>Update Profile pic</span>
            <input type="file"
              onChange={(e) => uploadprofilepic(e.target.files[0])} />
          </div>
          <div className="file-path-wrapper">
            <input className="file-path validate" type="text" />
          </div>

        </div>
            </div>
                <div>
                    <h4>{state?state.name:"loading"}</h4>
                    <h4>{state?state.email:"loading"}</h4>
                    <div style={{ display: "flex", justifyContent: 'space-between', width: '120%' }}>
                        <h5>{state?mypics.length:'loading'} Posts</h5>
                        <h5>{state?state.followers.length:'loading'}Followers </h5>
                        <h5>{state?state.following.length:'loading'} Following</h5>

                    </div>
                </div>

            </div>
            <div className="gallery">
                {
                mypics.map(item=>{
                    return( 
                         <img key={item._id} className="item" src={item.photo}
                    alt={item.title} /> 
                    )
                })
            }
                
               
            </div>
        </div>
    )
}

export default Profile