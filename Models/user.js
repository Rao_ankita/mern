const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema.Types
const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    pic:{
        type:String,
        default:'https://res.cloudinary.com/ankita1297/image/upload/v1590556247/defaultpic_znezyw.png'
    },
    followers:[
        {type:ObjectId,
            ref:"user"
            }
    ],
    following:[
        {type:ObjectId,
        ref:"user"
        }
    ]

})
mongoose.model('user',userSchema)