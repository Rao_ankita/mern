const express = require('express');
const app = express()
const PORT = process.env.PORT||5000;
const mongoose= require ('mongoose');
const {mongoURI} = require('./config/key')


const db = require('./config/key').mongoURL;
require('./Models/user')
mongoose.connect(db,{useNewUrlParser:true,useUnifiedTopology: true })
.then(()=> console.log('mongodb conected'))
.catch(err =>console.log(err));
 require('./Models/user')
 require('./Models/post')

 app.use(express.json())
 app.use(require('./routes/auth'))
 app.use(require('./routes/post'))
 app.use(require('./routes/user'))

 if(process.env.NODE_ENV=="production")
 {
     app.use(express.static('react-app/build'))
     const path = require('path')
     app.get('*',(req,res)=>{
         res.sendFile(path.resolve(__dirname,'react-app','build','index.html'))
     })
 }
 

app.listen(PORT,()=>console.log(`server is listen at`,PORT));

